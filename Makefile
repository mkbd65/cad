.PHONY: main keys_en-us-intl keys_de help

.DEFAULT_GOAL := help

main:  ## generate main case
	# TODO: split into printable pieces
	#openscad --export-format binstl -o main.stl main.scad
	openscad -o main.3mf main.scad

keyset_en-us-intl:  ## generate key caps with english us interational layout
	#openscad --export-format binstl -o keyset_en-us-intl.stl keyset_en-us-intl.scad
	openscad -o keyset_en-us-intl.3mf keyset_en-us-intl.scad

keyset_de:  ## generate key caps with german layout
	#openscad --export-format binstl -o keyset_de.stl keyset_de.scad
	openscad -o keyset_de.3mf keyset_de.scad

help:  ## display this help
	@grep -Fh "##" $(MAKEFILE_LIST) | grep -Fv fgrep | sed -e 's/\\$$//' | sed -e 's/##//'
