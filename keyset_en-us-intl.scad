// en-us-intl layout

$fn = 64;

use <lib/mat.scad>;
use <lib/key.scad>;

mat(rows=13, columns=5);

key(x=0,  y=0, label="Ctrl", size=2.5);
key(x=1,  y=0, label="Fn");
key(x=2,  y=0, label="⬆", size=5);  // Shift
key(x=3,  y=0, label="Alt", size=3);
key(x=4,  y=0, w=3, label="—————");  // Space
key(x=7,  y=0, label="-_");
key(x=8,  y=0, label="= +", spacing=0.6);
key(x=9,  y=0, label="◀");
key(x=10, y=0, label="▼");
key(x=11, y=0, label="▶");
key(x=12, y=0, label="Ctrl", size=2.5);

key(x=0,  y=1, label="Z");
key(x=1,  y=1, label="X");
key(x=2,  y=1, label="C");
key(x=3,  y=1, label="V");
key(x=4,  y=1, label="B");
key(x=5,  y=1, label="N");
key(x=6,  y=1, label="M");
key(x=7,  y=1, label=",<");
key(x=8,  y=1, label=".>");
key(x=9,  y=1, label="/?");
key(x=10, y=1, label="▲");
key(x=11, y=1, w=2, label="⬆", size=5);  // Shift

key(x=0,  y=2, label="A");
key(x=1,  y=2, label="S");
key(x=2,  y=2, label="D");
key(x=3,  y=2, label="F", marker=true);
key(x=4,  y=2, label="G");
key(x=5,  y=2, label="H");
key(x=6,  y=2, label="J", marker=true);
key(x=7,  y=2, label="K");
key(x=8,  y=2, label="L");
key(x=9,  y=2, label=";:");
key(x=10, y=2, label="'\"");
key(x=11, y=2, w=2, label="↵", size=5);  // Return

key(x=0,  y=3, label="Q");
key(x=1,  y=3, label="W");
key(x=2,  y=3, label="E");
key(x=3,  y=3, label="R");
key(x=4,  y=3, label="T");
key(x=5,  y=3, label="Y");
key(x=6,  y=3, label="U");
key(x=7,  y=3, label="I");
key(x=8,  y=3, label="O");
key(x=9,  y=3, label="P");
key(x=10, y=3, label="[{");
key(x=11, y=3, label="]}");
key(x=12, y=3, label="\\|");

key(x=0,  y=4, label="Esc", size=3);
key(x=1,  y=4, label="1!");
key(x=2,  y=4, label="2@");
key(x=3,  y=4, label="3#");
key(x=4,  y=4, label="4$");
key(x=5,  y=4, label="5%");
key(x=6,  y=4, label="6^");
key(x=7,  y=4, label="7&");
key(x=8,  y=4, label="8*");
key(x=9,  y=4, label="9(");
key(x=10, y=4, label="0)");
key(x=11, y=4, w=2, label="⌫", size=5);  // backspace

