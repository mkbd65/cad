$fn = 64;

radius = 7;
pcb_width = 150.7;
pcb_height = 64.1;
margin = 2;
thickness = 1;

difference() {
  union() {
    // base shape
    hull() {
      cylinder(h=thickness, r=radius);
      translate([pcb_width+2*margin-2*radius,0,0]) cylinder(h=thickness, r=radius);
      translate([0,pcb_height+2*margin-2*radius,0]) cylinder(h=thickness, r=radius);
      translate([pcb_width+2*margin-2*radius,pcb_height+2*margin-2*radius,0]) cylinder(h=thickness, r=radius);
    }
    // nodges
    translate([10, -radius, 0]) cylinder(h=thickness, d=10);
    translate([(pcb_width+2*margin)/2-radius, -radius, 0]) cylinder(h=thickness, d=10);
    translate([pcb_width+2*margin-2*radius-10, -radius, 0]) cylinder(h=thickness, d=10);
    translate([10, pcb_height+2*margin-radius, 0]) cylinder(h=thickness, d=10);
    translate([(pcb_width+2*margin)/2-radius, pcb_height+2*margin-radius, 0]) cylinder(h=thickness, d=10);
    translate([pcb_width+2*margin-2*radius-10, pcb_height+2*margin-radius, 0]) cylinder(h=thickness, d=10);
  }
  // screw holes
  translate([10, -radius-1.5, 0]) cylinder(h=thickness, d=2);
  translate([(pcb_width+2*margin)/2-radius, -radius-1.5, 0]) cylinder(h=thickness, d=2);
  translate([pcb_width+2*margin-2*radius-10, -radius-1.5, 0]) cylinder(h=thickness, d=2);
  translate([10, pcb_height+2*margin-radius+1.5, 0]) cylinder(h=thickness, d=2);
  translate([(pcb_width+2*margin)/2-radius, pcb_height+2*margin-radius+1.5, 0]) cylinder(h=thickness, d=2);
  translate([pcb_width+2*margin-2*radius-10, pcb_height+2*margin-radius+1.5, 0]) cylinder(h=thickness, d=2);
  // usb hole
  translate([82-radius,-radius-10,0]) cube([18, 20, thickness]);
}