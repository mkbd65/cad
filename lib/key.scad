// Generate a key cap at a specific position in the matrix
module key(x, y, w=1, h=1, label="", spacing=1, size=3.5, marker=false) {
  include <key_specs.scad>;
  $fn = 64;
  wall_thickness = 1;

  color("DarkSlateGray") translate([x*key_width + x*padding + margin, y*key_height + y*padding + margin, mat_thickness]) difference() {
    union() {
      hull() {
        translate([key_radius, key_radius, 0]) cylinder(h=key_thickness, r=key_radius);
        translate([key_radius, key_height*h + padding*(h-1) - key_radius, 0]) cylinder(h=key_thickness, r=key_radius);
        translate([key_width*w + padding*(w-1) - key_radius, key_radius, 0]) cylinder(h=key_thickness, r=key_radius);
        translate([key_width*w + padding*(w-1) - key_radius, key_height*h + padding*(h-1) - key_radius, 0]) cylinder(h=key_thickness, r=key_radius);
      }
    }

    // indent instead of dome
    translate([0,0,key_thickness]) hull() {
        translate([1.1, 1.1, 0]) scale([1,1,0.3]) sphere(r=1);
        translate([key_width*w + padding*(w-1) - 1.1, 1.1, 0]) scale([1,1,0.3]) sphere(r=1);
        translate([1.1, key_height*h + padding*(h-1) - 1.1, 0]) scale([1,1,0.3]) sphere(r=1);
        translate([key_width*w + padding*(w-1) - 1.1, key_height*h + padding*(h-1) - 1.1, 0]) scale([1,1,0.3]) sphere(r=1);
    }

    // depth of 0.3mm+
    // Noto Sans Mono:style=Medium
    // subtract label from key cap
    translate([(key_width*w + padding*(w-1))/2 -(0.5/spacing-0.5), (key_height*h + padding*(h-1))/2, key_thickness-0.5]) linear_extrude(height=1) text(label, size=size, font="DejaVu Sans Mono:style=Bold", halign="center", valign="center", spacing=spacing, $fn=64);

    // stem cutout
    hull() {
      translate([key_radius+wall_thickness, key_radius+wall_thickness, 0]) cylinder(h=key_thickness-1, r=key_radius);
      translate([key_radius+wall_thickness, key_height*h + padding*(h-1) - key_radius - wall_thickness, 0]) cylinder(h=key_thickness-1, r=key_radius);
      translate([key_width*w + padding*(w-1) - key_radius-wall_thickness, key_radius + wall_thickness, 0]) cylinder(h=key_thickness-1, r=key_radius);
      translate([key_width*w + padding*(w-1) - key_radius - wall_thickness, key_height*h + padding*(h-1) - key_radius - wall_thickness, 0]) cylinder(h=key_thickness-1, r=key_radius);
    }
  }

  // stem
  hull() {
    translate([x*key_width + x*padding + margin + key_width/2-key_radius, y*key_height + y*padding + margin + key_height/2, mat_thickness]) cylinder(h=key_thickness-1, r=key_radius);
    translate([x*key_width + x*padding + margin + key_width/2+key_radius, y*key_height + y*padding + margin + key_height/2, mat_thickness]) cylinder(h=key_thickness-1, r=key_radius);
  }

  // marker for f anf j key
  if (marker) translate([x*key_width + x*padding + margin + key_width-1.5*key_radius, y*key_height + y*padding + margin + key_height/2, mat_thickness+key_thickness-1]) cylinder(h=key_thickness-1, d=key_radius);
}

