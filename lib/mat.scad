// genrate key mat that sits under the key caps

module mat(rows=13, columns=5) {
  include <key_specs.scad>;
  $fn = 64;
  wall_thickness = 1;

  color("DarkSlateGray") cube([rows*key_width + (rows-1)*padding + 2*margin, columns*key_height + (columns-1)*padding + 2*margin, mat_thickness]);
}

