$fn = 64;

pcb_radius = 5;
pcb_width = 147;
pcb_height = 65;
pcb_thickness = 1;

module switch() {
  thickness = 0.8;
  width = 6;
  height = 4;
  
  color("white") linear_extrude(thickness*0.7) polygon([[-width/2, 2.3/2], [-4.5/2, height/2], [4.5/2, height/2], [width/2, 2.3/2], [width/2, -2.3/2], [4.5/2, -height/2], [-4.5/2, -height/2], [-width/2, -2.3/2]]);
  color("black") cylinder(h=thickness, d=1.2);
}

module pcb_body() {
  color("purple") difference() {
    hull() {
      translate([-pcb_width/2+pcb_radius, -pcb_height/2+pcb_radius, 0]) cylinder(h=pcb_thickness, r=pcb_radius);
      translate([pcb_width/2-pcb_radius, -pcb_height/2+pcb_radius, 0]) cylinder(h=pcb_thickness, r=pcb_radius);
      translate([pcb_width/2-pcb_radius, pcb_height/2-pcb_radius, 0]) cylinder(h=pcb_thickness, r=pcb_radius);
      translate([-pcb_width/2+pcb_radius, pcb_height/2-pcb_radius, 0]) cylinder(h=pcb_thickness, r=pcb_radius);
    }
  }
  
  // switches
  cols = 5;
  rows = 13;
  switch_spacing_h = 10;
  switch_spacing_v = 7;
  translate([-1*(rows*switch_spacing_h)/2+switch_spacing_h/2, -61+pcb_height/2, 0]) for(x=[0:rows-1]) {
    for(y=[0:cols-1]) {
      translate([x*switch_spacing_h, y*switch_spacing_v, pcb_thickness]) switch();
    }
  }
}
