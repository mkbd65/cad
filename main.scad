$fn = 64;

// replace with your phone
include <phones/s22plus.scad>;
// phone_width
// phone_length
// phone_thickness
// phone_corner_rad
// phone_body
// camera_bump

use <lib/pcb.scad>;

//phone_body();


// upper case
upper_case_thickness = 2;
difference() {
  translate([0,0,-upper_case_thickness]) hull() {
    translate([-phone_width/2+phone_corner_rad, phone_length/2-phone_corner_rad, 0]) cylinder(h=upper_case_thickness, r=phone_corner_rad);
    translate([-phone_width/2+phone_corner_rad, -phone_length/2+phone_corner_rad, 0]) cylinder(h=upper_case_thickness, r=phone_corner_rad);
    translate([phone_width/2-phone_corner_rad, phone_length/2-phone_corner_rad, 0]) cylinder(h=upper_case_thickness, r=phone_corner_rad);
    translate([phone_width/2-phone_corner_rad, -phone_length/2+phone_corner_rad, 0]) cylinder(h=upper_case_thickness, r=phone_corner_rad);
  }
  camera_bump();
}
// lower case