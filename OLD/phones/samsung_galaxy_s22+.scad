// Samsung Galaxy S22+

$fn = 64;

module SamsungGalaxyS22Plus() {
    depth = 7.6;
    width = 75.8;
    height = 157.4;
    corner_radius = 10;

    bump_depth = 1.5;
    bump_width = 22;
    bump_height = 50;

    flash_radius = 3;
    flash_top = 11;
    flash_left = 27;

    translate([corner_radius, corner_radius, 0]) {
        // phone body
        hull() {
            cylinder(h=depth, r=corner_radius);
            translate([width - 2*corner_radius, 0, 0]) cylinder(h=depth, r=corner_radius);
            translate([0, height - 2*corner_radius, 0]) cylinder(h=depth, r=corner_radius);
            translate([width - 2*corner_radius, height - 2*corner_radius, 0]) cylinder(h=depth, r=corner_radius);
        };

        translate([0, height - 2*corner_radius, depth]) {
            // camera bump
            hull() {
                cylinder(h=bump_depth, r1=corner_radius, r2=corner_radius-1);
                translate([bump_width - 2*corner_radius + corner_radius-2, corner_radius-2, 0]) cylinder(h=bump_depth, r1=2, r2=1);
                translate([bump_width - 2*corner_radius, -1 * (bump_height - 2*corner_radius), 0]) cylinder(h=bump_depth, r1=corner_radius, r2=corner_radius-1);
                translate([-1* (corner_radius-2), -1* (bump_height - 2*corner_radius + corner_radius-2), 0]) cylinder(h=bump_depth, r1=2, r2=1);
            };
            
            // flashlight
            translate([flash_left - corner_radius, -1* (flash_top - corner_radius), 0]) cylinder(h=10, r1=flash_radius, r2=10+flash_radius);
        };
    };
};

SamsungGalaxyS22Plus();