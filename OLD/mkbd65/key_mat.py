from typing import Tuple

from solid import OpenSCADObject, cube, linear_extrude, text, translate, scale, rotate, cylinder, union, color, square, offset
from solid.utils import up

from mkbd65.util import get_label_size, FILAMENT_LAYER, LAYOUTS


def key(
    width: float,
    height: float,
    corner_radius: float,
    thickness: float,
    font: str,
    label: str = ""
) -> OpenSCADObject:
    """
    Generates a single key

    key:
    +-----+
    |     | height
    +-----+
    width \\ thickness

    :param width: Width in mm
    :param height: Height in mm
    :param corner_radius: Radius of corners in mm
    :param thickness: Thickness in mm
    :param label: Text label
    :param font: Font to use
    :return:
    """
    dome_height = 2

    return union()(
        translate(v=(0, height / 2, 2))(
            scale(v=(1, 1, dome_height / height))(
                rotate((0, 90, 0))(
                    cylinder(h=width, d=height)
                )
            ) * translate(v=(width / 2, height / 2, 0))(
                scale((1, 1, dome_height / width))(
                    rotate((90, 0, 0))(
                        cylinder(h=height, d=width)
                    )
                )
            )
        ) * up(z=2)(
            linear_extrude(height=dome_height / 2)(
                translate(v=(corner_radius, corner_radius, 0))(
                    offset(r=corner_radius)(
                        square(size=(width - 2 * corner_radius, height - 2 * corner_radius))
                    )
                )
            )
        ),
        linear_extrude(height=2)(
            # round_rect(width=width, height=height, radius=corner_radius)
            translate(v=(corner_radius, corner_radius, 0))(
                offset(r=corner_radius)(
                    square(size=(width - 2 * corner_radius, height - 2 * corner_radius))
                )
            )
        )
    ) - translate(v=(width / 2, height / 2, thickness + dome_height / 2 - dome_height * 3 * FILAMENT_LAYER))(
        linear_extrude(height=dome_height * 3 * FILAMENT_LAYER)(
            text(text=label, size=get_label_size(label), font=font, halign='center', valign='center')
        )
    )


def key_mat(
    margin: float | Tuple[float, float, float, float],
    padding: float | Tuple[float, float],
    thickness: float,
    key_width: float,
    key_height: float,
    key_radius: float,
    key_thickness: float,
    layout: str,
    variant: str,
    font: str,
    keys_h: int = 13,
    keys_v: int = 5
) -> OpenSCADObject:
    """
    Generates elastic mat with fixed layout but variable sizes

    mat:
    +---------------------------------------------+
    |      margin                                 |
    |      +-----+       +-----+     +-----+      |
    |margin|     |padding|     | ... |     |margin|
    |      +-----+       +-----+     +-----+      |
    |      padding                                |
    |      +-----+       +-----+     +-----+      |
    |      |     |       |     | ... |     |      |
    |      +-----+       +-----+     +-----+      |
    |        ...           ...         ...        |
    |      +-----+       +-----+     +-----+      |
    |      |     |       |     | ... |     |      |
    |      +-----+       +-----+     +-----+      |
    |      margin                                 |
    +---------------------------------------------+
                                                   \\ thickness

    :param keys_v:
    :param keys_h:
    :param variant:
    :param margin: left, right, top and bottom margin, can be single number to set all at once.
    :param padding: Horizontal and vertical padding, can be single number to set both at once.
    :param thickness: Thickness of matt.
    :param key_width: Width of keys.
    :param key_height: Height of keys.
    :param key_radius: Radius of key corners.
    :param key_thickness: Thickness of keys without matt.
    :param layout: ansi or iso layout.
    :param font:
    :return:
    """
    spacebar_start = 4  # horizontal start key of space bar (0 index)
    spacebar_width = 3  # space bar length in keys

    margin_l = margin[0] if isinstance(margin, Tuple) else margin
    margin_r = margin[1] if isinstance(margin, Tuple) else margin
    margin_t = margin[2] if isinstance(margin, Tuple) else margin
    margin_b = margin[3] if isinstance(margin, Tuple) else margin

    padding_h = padding[0] if isinstance(padding, Tuple) else padding
    padding_v = padding[1] if isinstance(padding, Tuple) else padding

    mat = cube([
        keys_h * key_width + (keys_h - 1) * padding_h + margin_l + margin_r,
        keys_v * key_height + (keys_v - 1) * padding_v + margin_t + margin_b,
        thickness
    ])

    # add keys to mat
    for x in range(keys_h):
        for y in range(keys_v):
            if (x == keys_h - 1 and (y in [0, 2, 3] and layout.startswith("ansi") or y in [0, 2] and layout == "iso")) \
                    or (spacebar_start < x < spacebar_start + spacebar_width and y == keys_v - 1):
                # skip for wide keys (backspace, enter, shift and space bar)
                continue
            elif x == keys_h - 1 and y == 1 and layout in ["ansi_big_enter", "iso"]:
                # big enter
                mat += translate(v=(padding_h + x * (key_width + padding_h), padding_v + (keys_v - y - 2) * (key_height + padding_v), thickness))(key(
                    width=key_width,
                    height=2 * key_height + padding_v,
                    corner_radius=key_radius,
                    thickness=key_thickness,
                    font=font,
                    label=LAYOUTS[f"{layout}_{variant}"][y][x]
                ))
            elif x == keys_h - 2 and (y == 0 or (y == 2 and layout == "ansi") or (y == 3 and layout != "iso")):
                # wide key
                mat += translate(v=(padding_h + x * (key_width + padding_h), padding_v + (keys_v - y - 1) * (key_height + padding_v), thickness))(key(
                    width=2 * key_width + padding_h,
                    height=key_height,
                    corner_radius=key_radius,
                    thickness=key_thickness,
                    font=font,
                    label=LAYOUTS[f"{layout}_{variant}"][y][x]
                ))
            elif x == spacebar_start and y == keys_v - 1:
                # space bar
                mat += translate(v=(padding_h + x * (key_width + padding_h), padding_v + (keys_v - y - 1) * (key_height + padding_v), thickness))(key(
                    width=spacebar_width * key_width + (spacebar_width - 1) * padding_h,
                    height=key_height,
                    corner_radius=key_radius,
                    thickness=key_thickness,
                    font=font,
                    label=LAYOUTS[f"{layout}_{variant}"][y][x]
                ))
            else:
                # normal key
                mat += translate(v=(padding_h + x * (key_width + padding_h), padding_v + (keys_v - y - 1) * (key_height + padding_v), thickness))(key(
                    width=key_width,
                    height=key_height,
                    corner_radius=key_radius,
                    thickness=key_thickness,
                    font=font,
                    label=LAYOUTS[f"{layout}_{variant}"][y][x]
                ))

    # add holes to keys for better "pressability"
    for x in range(keys_h):
        for y in range(keys_v):
            if (x == keys_h - 1 and (y in [0, 2, 3] and layout.startswith("ansi") or y in [0, 2] and layout == "iso")) \
                    or (spacebar_start < x < spacebar_start + spacebar_width and y == keys_v - 1):
                # skip for wide keys (backspace, enter, shift and space bar)
                continue
            elif x == keys_h - 1 and y == 1 and layout in ["ansi_big_enter", "iso"]:
                # big enter
                mat -= translate(v=(padding_h + x * (key_width + padding_h) + 1, padding_v + (keys_v - y - 2) * (key_height + padding_v) + 1, 0))(
                    cube((key_width - 2, 2 * key_height + padding_v - 2, key_thickness + thickness - 2 * FILAMENT_LAYER)) - translate(v=(key_width / 2 - 1, key_height / 2 - 1, 0))(
                        cylinder(d=2, h=key_thickness + thickness - 2 * FILAMENT_LAYER)
                    ) - translate(v=(key_width / 2 - 1, key_height + padding_v + key_height / 2 - 1, 0))(
                        cylinder(d=2, h=key_thickness + thickness - 2 * FILAMENT_LAYER)
                    ) - translate(v=(key_width / 2 - 2, key_height / 2 - 1, 0))(
                        cube((2, padding_v + key_height, key_thickness + thickness - 2 * FILAMENT_LAYER))
                    )
                )
            elif x == keys_h - 2 and (y == 0 or (y == 2 and layout == "ansi") or (y == 3 and layout != "iso")):
                # wide key
                mat -= translate(v=(padding_h + x * (key_width + padding_h) + 1, padding_v + (keys_v - y - 1) * (key_height + padding_v) + 1, 0))(
                    cube((2 * key_width + padding_h - 2, key_height - 2, key_thickness + thickness - 2 * FILAMENT_LAYER)) - translate(v=(key_width / 2 - 1, key_height / 2 - 1, 0))(
                        cylinder(d=2, h=key_thickness + thickness - 2 * FILAMENT_LAYER)
                    ) - translate(v=(key_width + padding_h + key_width / 2 - 1, key_height / 2 - 1, 0))(
                        cylinder(d=2, h=key_thickness + thickness - 2 * FILAMENT_LAYER)
                    ) - translate(v=(key_width / 2 - 1, key_height / 2 - 2, 0))(
                        cube((padding_h + key_width, 2, key_thickness + thickness - 2 * FILAMENT_LAYER))
                    )
                )
            elif x == spacebar_start and y == keys_v - 1:
                # space bar
                mat -= translate(v=(padding_h + x * (key_width + padding_h) + 1, padding_v + (keys_v - y - 1) * (key_height + padding_v) + 1, 0))(
                    cube((spacebar_width * key_width + (spacebar_width - 1) * padding_h - 2, key_height - 2, key_thickness + thickness - 2 * FILAMENT_LAYER)) - translate(v=(key_width / 2 - 1, key_height / 2 - 1, 0))(
                        cylinder(d=2, h=key_thickness + thickness - 2 * FILAMENT_LAYER)
                    ) - translate(v=((spacebar_width - 1) * key_width + (spacebar_width - 1) * padding_h + key_width / 2 - 1, key_height / 2 - 1, 0))(
                        cylinder(d=2, h=key_thickness + thickness - 2 * FILAMENT_LAYER)
                    ) - translate(v=(key_width / 2 - 1, key_height / 2 - 2, 0))(
                        cube(((spacebar_width - 1) * padding_h + (spacebar_width - 1) * key_width, 2, key_thickness + thickness - 2 * FILAMENT_LAYER))
                    )
                )
            else:
                # normal key
                mat -= translate(v=(padding_h + x * (key_width + padding_h) + 1, padding_v + (keys_v - y - 1) * (key_height + padding_v) + 1, 0))(
                    cube((key_width - 2, key_height - 2, key_thickness + thickness - 2 * FILAMENT_LAYER)) - translate(v=(key_width / 2 - 1, key_height / 2 - 1, 0))(
                        cylinder(d=2, h=key_thickness + thickness - 2 * FILAMENT_LAYER)
                    )
                )

    return color("DarkSlateGray")(mat)
