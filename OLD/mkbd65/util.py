PCB_RADIUS = 5
PCB_WIDTH = 150
PCB_HEIGHT = 64
FILAMENT_LAYER = 0.1

# TODO: Add iso_fr (AZERTY) just for Slim :P
LAYOUTS = {
    "ansi_en": [
        ["Esc", "1", "2", "3", "4", "5", "6", "7", "8", "9", "0", "⌫", None],
        ["Q", "W", "E", "R", "T", "Y", "U", "I", "O", "P", "[", "]", "\\\\"],
        ["A", "S", "D", "F", "G", "H", "J", "K", "L", ";", "'", "↵", None],
        ["Z", "X", "C", "V", "B", "N", "M", ",", ".", "/", "▲", "⬆", None],
        ["Ctrl", "Fn", "⬆", "Alt", "———", None, None, "-", "=", "◀", "▼", "▶", "Ctrl"]
    ],
    "ansi_big_enter_en": [
        ["Esc", "1", "2", "3", "4", "5", "6", "7", "8", "9", "0", "⌫", None],
        ["Q", "W", "E", "R", "T", "Y", "U", "I", "O", "P", "[", "]", "↵"],
        ["A", "S", "D", "F", "G", "H", "J", "K", "L", ";", "'", "\\\\", None],
        ["Z", "X", "C", "V", "B", "N", "M", ",", ".", "/", "▲", "⬆", None],
        ["Ctrl", "Fn", "⬆", "Alt", "———", None, None, "-", "=", "◀", "▼", "▶", "Ctrl"]
    ],
    "iso_en": [
        ["Esc", "1", "2", "3", "4", "5", "6", "7", "8", "9", "0", "⌫", None],
        ["Q", "W", "E", "R", "T", "Y", "U", "I", "O", "P", "[", "]", "↵"],
        ["A", "S", "D", "F", "G", "H", "J", "K", "L", ";", "'", "#", None],
        ["Z", "X", "C", "V", "B", "N", "M", ",", ".", "/", "▲", "\\\\", "⬆"],
        ["Ctrl", "Fn", "⬆", "Alt", "———", None, None, "-", "=", "◀", "▼", "▶", "Ctrl"]
    ],
    "iso_de": [
        ["Esc", "1", "2", "3", "4", "5", "6", "7", "8", "9", "0", "⌫", None],
        ["Q", "W", "E", "R", "T", "Z", "U", "I", "O", "P", "Ü", "+", "↵"],
        ["A", "S", "D", "F", "G", "H", "J", "K", "L", "Ö", "Ä", "#", None],
        ["Y", "X", "C", "V", "B", "N", "M", ",", ".", "/", "▲", "<", "⬆"],
        ["Ctrl", "Fn", "⬆", "Alt", "———", None, None, "ẞ", "´", "◀", "▼", "▶", "Ctrl"]
    ]
}


def get_label_size(label: str) -> float:
    """
    Get font size depending on label.

    :param label:
    :return:
    """
    if label == "↵":
        return 4
    elif label in ["Esc", "Alt", "Fn", "Ä", "Ü", "Ö"]:
        return 3
    elif label == "Ctrl":
        return 2.5
    else:
        return 3.5
