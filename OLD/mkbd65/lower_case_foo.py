"""
All sizes are in millimeters.
"""

from solid import OpenSCADObject, hull, translate, cylinder, intersection, sphere, scale


def lower_case(width: float, height: float) -> OpenSCADObject:
    """
    Generates lower half of the case

    :param width: length from top to bottom (landscape)
    :param height: length from left to right (landscape)
    :return:
    """
    length = 160.7
    width = 74.1
    height = 5
    radius = 5

    corners = [(-1, -1), (-1, 1), (1, -1), (1, 1)]

    for x, y in corners:


    hull()(
        [scale(v=(sx, sy, 1))(
            # upper cylindrical part
            translate(v=(length / 2 - radius, width / 2 - radius, 0))(
                cylinder(r=radius, h=height - radius)
            ) if height > radius else None,
            translate(v=(length / 2 - radius, width / 2 - radius, height - radius))(
                sphere(r=radius)
            ) * translate(v=(length / 2 - radius, width / 2 - radius, 0))(
                cylinder(r=radius, h=height)
            ) if height < 2 * radius else None
        ) for sx in [-1, 1]]
    )


    for sx in [-1, 1]:
        for sy in [-1, 1]:
            scale(v=(sx, sy, 1))(
                # upper cylindrical part
                if height > radius:
                    translate(v=(length / 2 - radius, width / 2 - radius, 0))(
                        cylinder(r=radius, h=height - radius)
                    )
                # upper spheric part
                intersection()(
                    # sphere
                    translate(v=(length / 2 - radius, width / 2 - radius, height - radius))(
                        sphere(r=radius)
                    ),
                    # clip sphere at bottom plane
                    translate(v=(length / 2 - radius, width / 2 - radius, 0))(
                        cylinder(r=radius, h=height)
                    ) if height < 2 * radius else None
                )
            )

    return hull()(


    )
    return linear_extrude(height=thickness)(
        round_rect(width=width, height=height, radius=corner_radius)
    )




//hull() {
//  for(sx=[-1,1]) {
//    for(sy=[-1,1]) {
//      scale([sx,sy,1]) {
//        // lower cylindric part
//        /*if(height>radius) {
//          translate([length/2-radius,width/2-radius,0]) {
//            cylinder(r=radius,h=height-radius);
//          }
//        }*/
//        // upper spheric part
//        intersection() {
//          // sphere
//          translate([length/2-radius,width/2-radius,height-radius]) {
//            sphere(r=radius);
//          }
//          // clip sphere at bottom plane
//          if(height<2*radius) {
//            translate([length/2-radius,width/2-radius,0]) {
//              cylinder(r=radius,h=height);
//            }
//          }
//        }
//      }
//    }
//  }
//}