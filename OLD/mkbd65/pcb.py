from solid import OpenSCADObject, color, linear_extrude, polygon, translate, scale, offset, square, union, cylinder

from mkbd65.util import PCB_WIDTH, PCB_HEIGHT, PCB_RADIUS


def switch() -> OpenSCADObject:
    """
    Alps SKTKABE010

      6 +----+ 5
       /      \
    7 +        + 4  | 2mm
    8 +        + 3  |
       \  4mm /
      1 +----+ 2

    :return:
    """
    thickness = 0.78

    return color("Silver")(
        linear_extrude(height=0.5)(
            polygon(points=[(1, 0), (5, 0), (6, 1), (6, 3), (5, 4), (1, 4), (0, 3), (0, 1)]),
        )
    ) + color("Black")(
        translate(v=(3, 2, 0))(
            cylinder(r=0.5, h=thickness)
        )
    )


def pcb(key_width: float, key_height: float, key_padding: float) -> OpenSCADObject:
    """

    :return:
    """
    pcb_thickness = 1
    rows = 5
    columns = 13

    board = color(c="Purple")(
        translate(v=(PCB_RADIUS, PCB_RADIUS, pcb_thickness/2))(
            scale(v=(1, 1, pcb_thickness))(
                offset(r=PCB_RADIUS)(
                    square([PCB_WIDTH - 2 * PCB_RADIUS, PCB_HEIGHT - 2 * PCB_RADIUS])
                )
            )
        )
    )

    switches = []
    margin_x = (PCB_WIDTH - (columns * key_width + (columns - 1) * key_padding)) / 2 + (key_width - 6) / 2
    margin_y = 3

    for x in range(columns):
        for y in range(rows):
            switches.append(
                translate(v=(margin_x + x * (key_width + key_padding), margin_y + y * (key_height + key_padding), pcb_thickness))(
                    switch()
                )
            )

    return union()(
        board,
        *switches
    )
    # TODO: Add ATMEGA and flat cable connector

    # rows = 5
    # columns = 12
    # hspacing = 4
    # vspacing = 3
    # width = 6
    # height = 4
    #
    # board = color("Green")(cube([123, 49, 0.6])) + translate((13, 36, 0.6))(
    #     color("BlackPaint")(cube([7, 7, 1]))
    # ) + translate((23, 35.75, 0.6))(
    #     color("BlackPaint")(cube([2.5, 2, 0.55]))
    # )
    #
    # for row in range(rows):
    #     for col in range(columns):
    #         board += translate((3.5 + (hspacing + width) * col, 2 + (vspacing + height) * row, 0.6))(
    #             switch()
    #         )
    #
    # return board
