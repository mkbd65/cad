from solid import OpenSCADObject, translate, minkowski, cube, cylinder, linear_extrude, hull, circle, square


def _generic_bump(width: float, height: float, thickness: float, radius: float) -> OpenSCADObject:
    """
    A rounded rectangular bump

    :return:
    """
    return translate(v=(radius, -1 * height + radius, 0))(
        minkowski()(
            cube([width - 2 * radius, height - 2 * radius, thickness]),
            cylinder(r=radius, h=thickness)
        )
    )


def oneplus_8t() -> OpenSCADObject:
    """
    Camera bump of the Oneplus 8T

    :return:
    """
    return translate(v=(6, 7, 0))(
        _generic_bump(width=39, height=25, thickness=1, radius=5)
    )


def samsung_s22plus() -> OpenSCADObject:
    """
    Camra bump of the Samsung S22+

    :return:
    """
    return linear_extrude(height=1)(
        hull()(
            circle(r=10),
            translate(v=(20, 40, 0))(
                circle(r=10)
            ),
            translate(v=(20, 0, 0))(
                square(size=(20, 20), center=True)
            ),
            translate(v=(0, 40, 0))(
                square(size=(20, 20), center=True)
            )
        )
    )
